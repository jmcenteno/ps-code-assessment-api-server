/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Customers', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    middleName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nickname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    ssn: {
      type: DataTypes.STRING,
      allowNull: false
    },
    titleId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    gender: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    maritalStatusId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Customers',
    freezeTableName: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {

        models.Customers.belongsTo(models.MaritalStatus, {
          as: 'maritalStatus',
          foreignKey: 'maritalStatusId'
        });

        models.Customers.belongsTo(models.Titles, { 
          as: 'title', 
          foreignKey: 'titleId'
        });

      }
    }
  });
};
