'use strict';

const wrap = require('co-express');
const Models = require('../../models');

module.exports = {

	getMaritalStatuses: wrap(function * (req, res, next) {
		
		try {

			let data = yield GetMaritalStatuses();

			return res.json(data);

		} catch(error) {
			
			res.status(500).send(error);
			
			return next();

		}

	}),

	getTitles: wrap(function * (req, res, next) {
		
		try {

			let data = yield GetTitles();

			return res.json(data);

		} catch(error) {
			
			res.status(500).send(error);
			
			return next();

		}

	}),


}


/**
 * Get all marital status records
 * @return {array} Collection of marital status objects
 */
function GetMaritalStatuses () {
	return new Promise((resolve, reject) => {

		Models.MaritalStatus.findAll({
			attributes: ['id', 'name']
		})
			.then(function (response) {

				return resolve(response);

			})
			.catch(function (error) {

				console.log(error);

				return reject(error);

			});

	});
}


/**
 * Get all title records
 * @return {array} Collection of title objects
 */
function GetTitles () {
	return new Promise((resolve, reject) => {

		Models.Titles.findAll({
			attributes: ['id', 'name']
		})
			.then(function (response) {

				return resolve(response);

			})
			.catch(function (error) {

				console.log(error);

				return reject(error);

			});

	});
}