'use strict';

const wrap = require('co-express');
const validator = require('validator');
const Models = require('../../models');

module.exports = {

	getAllCustomers: wrap(function * (req, res, next) {
		
		try {

			let customers = yield GetAllCustomers(req.query);

			return res.json(customers);

		} catch(error) {
			
			console.log(error);
			res.status(500).send(error);
			
			return next();

		}

	}),

	getCustomerById: wrap(function * (req, res, next) {
		
		try {

			let customer = yield GetCustomerById(req.params.id);

			return res.json(customer);

		} catch(error) {
			
			console.log(error);
			res.status(500).send(error);
			
			return next();

		}

	}),

	updateCustomer: wrap(function * (req, res, next) {
		
		try {

			let response = yield UpdateCustomer(req.params.id, req.body)
			console.log(response);
			
			return res.json(response);

		} catch (error) {
			
			console.log(error);
			res.status(500).send(error);
			
			return next();

		}

	}),

	deleteCustomer: wrap(function * (req, res, next) {
		
		try {

			let response = yield DeleteCustomer(req.params.id);
			console.log(response);

			return res.json(response);

		} catch (error) {
			
			console.log(error);
			res.status(500).send(error);
			
			return next();

		}

	}),

}


/**
 * Get all customer records
 * @return {array} Collection of customer objects
 */
function GetAllCustomers (query) {
	return new Promise((resolve, reject) => {

		let where = null;

		if (Object.keys(query).length) {
			
			where = {
				$or: {}
			}

			for (let key in query) {
				where.$or[key] = { 
					$like: '%' + query[key] + '%' 
				}
			}

		}

		Models.Customers.findAll({
			attributes: ['id', 'firstName', 'middleName', 'lastName', 'nickname'],
			include: [
      	{
      		model: Models.MaritalStatus,
      		as: 'maritalStatus',
      		attributes: ['name']
      	}
      ],
      where: where
		})
			.then(function (response) {
				
				return resolve(response);

			})
			.catch(function (error) {

				console.log(error);

				return reject(error);

			});

	});
}


/**
 * Get a single customer by its record ID
 * @param  {int} id Customer record ID
 * @return {object}    Customer object 
 */
function GetCustomerById (id) {
	return new Promise((resolve, reject) => {

		Models.Customers.findOne({
			where: {
				id: id
			},
			include: [
				{
      		model: Models.Titles,
      		as: 'title',
      		attributes: ['name']
      	}, {
      		model: Models.MaritalStatus,
      		as: 'maritalStatus',
      		attributes: ['name']
      	}
      ]
		})
			.then(function (response) {

				return resolve(response.dataValues);

			})
			.catch(function (error) {

				console.log(error);

				return reject(error);

			});

	});	
}


/**
 * Update a single customer record
 * @param {int} id    Customer record ID
 * @param {object} values Map of key/value pairs corresponding to the fields
 */
function UpdateCustomer (id, values) {
	return new Promise((resolve, reject) => {

		Models.Customers.update(values, {
			where: {
				id: id
			}
		})
			.then(function (response) {

				return resolve(response);

			})
			.catch(function (error) {

				console.log(error);

				return reject(error);

			});

	});
}


/**
 * Delete a single customer record
 * @param {int} id    Customer record ID
 */
function DeleteCustomer (id) {
	return new Promise((resolve, reject) => {

		Models.Customers.destroy({
			where: {
				id: id
			}
		})
			.then(function (response) {

				return resolve(response);

			})
			.catch(function (error) {

				console.log(error);

				return reject(error);

			});

	});
}