'use strict';

const router = require('express').Router();
const Controller = require('../controllers/customers');


/**
 * GET /customers
 * Get all customers
 */
router.get('/', Controller.getAllCustomers);


/**
 * GET /customers/:id
 * Get a single customer by its record ID
 * @param id int Customer record ID
 */
router.get('/:id', Controller.getCustomerById);


/**
 * PUT /customers/:id
 * Update a single customer record
 * @param id int Customer record ID
 */
router.put('/:id', Controller.updateCustomer);


/**
 * DELETE /customer/:id
 * Delete a single customer record
 * @param int Customer record ID
 */
router.delete('/:id', Controller.deleteCustomer);


module.exports = router;
