'use strict';

const router = require('express').Router();
const Controller = require('../controllers/options');

/**
 * GET /options/marital-status
 * Get all marital statuses
 */
router.get('/marital-status', Controller.getMaritalStatuses);


/**
 * GET /options/marital-status
 * Get all marital statuses
 */
router.get('/titles', Controller.getTitles);


module.exports = router;
