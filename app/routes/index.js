'use strict';

module.exports = function(app) {

	// customers
  app.use('/customers', require('./customers'));

  // options
  app.use('/options', require('./options'));

};
