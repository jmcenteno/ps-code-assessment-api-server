var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var cors = require('cors');
var port = process.env.PORT || 4000;

var app = express();

app.use(morgan('dev')); // log every request to the console

app.use(bodyParser.json()); // pull information from html in POST
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(cors()); // allow cross-origin requests

require('./app/routes')(app);

// index route
app.get('/', function(req, res) {
  return res
    .status(200)
    .set({ 'content-type': 'text/html; charset=utf-8' })
    .send('Welcome!');
});

// catch-all route
app.get('/*', function(req, res) {
  if (!res.headersSent) {
    return res
      .status(404)
      .set({ 'content-type': 'text/html; charset=utf-8' })
      .send('Not found');
  }
});

var server = app.listen(port, function() {
  console.log(
    'Listening at http://%s:%s', 
    server.address().address, 
    server.address().port
  );
});
