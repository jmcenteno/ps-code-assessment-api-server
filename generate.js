var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./data/db');

db.serialize(function() {

	db.run(`
  	CREATE TABLE IF NOT EXISTS MaritalStatus (
  		id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  		name varchar(50) default NULL,
  		createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  		updatedAt datetime DEFAULT NULL,
  		deletedAt datetime DEFAULT NULL
		)
	`);

	db.run(`
  	INSERT INTO MaritalStatus (name) 
  	VALUES 
  	('Single'),
  	('Married'),
  	('Divorced'),
  	('Widowed')
  `);

	db.run(`
  	CREATE TABLE IF NOT EXISTS Titles (
  		id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  		name varchar(50) NOT NULL,
  		createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  		updatedAt datetime DEFAULT NULL,
  		deletedAt datetime DEFAULT NULL
		)
	`);

	db.run(`
  	INSERT INTO Titles (name) 
  	VALUES 
  	('Mr.'),
  	('Ms.'),
  	('Mrs.')
  `);

	db.run(`
  	CREATE TABLE IF NOT EXISTS Customers (
  		id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  		firstName varchar(50) default NULL,
  		middleName varchar(50) default NULL,
  		lastName varchar(50) default NULL,
  		nickname varchar(50) default NULL,
  		ssn varchar(11) NOT NULL,
  		titleId INTEGER(11) default NULL,
  		gender char(1) default NULL,
  		maritalStatusId INTEGER(11) default NULL,
  		createdAt datetime NOT NULL,
  		updatedAt datetime DEFAULT NULL,
  		deletedAt datetime DEFAULT NULL
		)
	`);

  db.run(`
  	INSERT INTO Customers (firstName, middleName, lastName, nickname, ssn, titleId, gender, maritalStatusId, createdAt, updatedAt, deletedAt) 
  	VALUES 
		('Paul', 'Matthew', 'Ellis', 'William', '737-94-4418', 1, 'M', 3, '2016-03-10 20:52:04', NULL, NULL),
		('Gerald', 'Michael', 'Thomas', 'Joshua', '458-47-5846', 1, 'M', 2, '2016-01-05 00:43:17', NULL, NULL),
		('Patrick', 'Philip', 'White', 'Anthony', '305-33-7329', 3, 'M', 3, '2015-06-23 11:56:24', NULL, NULL),
		('Andrew', 'Johnny', 'Hudson', 'Mark', '418-67-9099', 2, 'M', 2, '2016-01-17 14:44:36', NULL, NULL),
		('Emily', 'Catherine', 'Morris', 'Melissa', '898-08-0429', 3, 'F', 3, '2016-05-26 20:19:55', NULL, NULL),
		('Amanda', 'Marilyn', 'Hart', 'Rebecca', '798-75-7049', 2, 'F', 3, '2016-01-04 00:46:23', NULL, NULL),
		('Arthur', 'Paul', 'Brooks', 'Raymond', '718-24-4178', 1, 'M', 2, '2015-07-20 13:05:41', NULL, NULL),
		('Alan', 'Jack', 'Reyes', 'Kevin', '739-35-5009', 1, 'M', 4, '2015-07-27 22:38:16', NULL, NULL),
		('Jane', 'Martha', 'Watson', 'Patricia', '332-99-0611', 2, 'F', 1, '2015-06-18 04:31:19', NULL, NULL),
		('Karen', 'Robin', 'Gibson', 'Cynthia', '894-98-1179', 3, 'F', 3, '2016-02-15 04:46:25', NULL, NULL),
		('Pamela', 'Phyllis', 'Garza', 'Catherine', '219-64-2796', 3, 'F', 3, '2015-08-06 07:27:52', NULL, NULL),
		('Walter', 'Chris', 'Miller', 'Antonio', '416-31-8328', 3, 'M', 2, '2015-09-04 09:54:25', NULL, NULL),
		('Deborah', 'Anne', 'Palmer', 'Rachel', '941-60-8870', 3, 'F', 3, '2015-08-08 06:14:18', NULL, NULL),
		('Shawn', 'Eugene', 'Phillips', 'Ralph', '755-74-0983', 2, 'M', 1, '2015-06-16 15:21:46', NULL, NULL),
		('Louise', 'Patricia', 'Moore', 'Nancy', '390-55-1130', 3, 'F', 2, '2015-12-13 10:48:48', NULL, NULL),
		('Alice', 'Betty', 'Lawson', 'Kathryn', '588-83-1417', 3, 'F', 1, '2015-07-30 13:47:57', NULL, NULL),
		('Janet', 'Julia', 'Lynch', 'Marilyn', '586-69-6071', 2, 'F', 2, '2016-04-09 02:59:11', NULL, NULL),
		('Kathryn', 'Rebecca', 'Hart', 'Patricia', '488-64-5385', 3, 'F', 1, '2015-08-31 08:03:58', NULL, NULL),
		('Karen', 'Katherine', 'Wright', 'Emily', '360-71-4299', 3, 'F', 1, '2016-04-03 11:01:39', NULL, NULL),
		('Robert', 'Ryan', 'Ward', 'Gregory', '142-33-2923', 2, 'M', 1, '2015-08-20 14:33:58', NULL, NULL),
		('Paul', 'Steve', 'Franklin', 'Joe', '204-93-6892', 2, 'M', 1, '2015-07-13 08:06:55', NULL, NULL),
		('Martha', 'Norma', 'Allen', 'Martha', '137-22-4041', 3, 'F', 2, '2015-12-13 03:01:05', NULL, NULL),
		('Gloria', 'Judy', 'Olson', 'Ruth', '172-04-9614', 3, 'F', 4, '2016-04-09 10:01:58', NULL, NULL),
		('Joe', 'Charles', 'Wilson', 'George', '222-75-3104', 2, 'M', 1, '2015-07-01 08:21:44', NULL, NULL),
		('Ruth', 'Wanda', 'Wheeler', 'Kathleen', '581-48-5714', 3, 'F', 3, '2015-11-27 01:32:05', NULL, NULL),
		('Thomas', 'Thomas', 'Gilbert', 'Fred', '305-23-7426', 1, 'M', 3, '2015-07-18 07:51:27', NULL, NULL),
		('Walter', 'Howard', 'Hamilton', 'Terry', '667-83-7937', 1, 'M', 3, '2015-09-13 12:36:08', NULL, NULL),
		('Phillip', 'Victor', 'Reid', 'Brian', '703-61-8226', 1, 'M', 3, '2015-11-04 18:50:11', NULL, NULL),
		('Jason', 'Jonathan', 'Kelly', 'Christopher', '378-66-2639', 1, 'M', 3, '2015-12-31 22:34:15', NULL, NULL),
		('Teresa', 'Irene', 'Evans', 'Helen', '213-10-9160', 3, 'F', 2, '2016-05-06 12:40:27', NULL, NULL),
		('Carl', 'Harry', 'Freeman', 'John', '890-64-0247', 3, 'M', 3, '2015-08-28 10:41:52', NULL, NULL),
		('Mark', 'Adam', 'Arnold', 'Steve', '989-84-1750', 3, 'M', 4, '2015-07-15 02:04:52', NULL, NULL),
		('Clarence', 'Philip', 'Franklin', 'Ryan', '275-88-7022', 3, 'M', 2, '2015-11-19 13:29:36', NULL, NULL),
		('Wanda', 'Nancy', 'Alexander', 'Irene', '941-84-3613', 1, 'F', 1, '2016-03-31 20:11:42', NULL, NULL),
		('Henry', 'Brian', 'Black', 'Nicholas', '366-19-1069', 1, 'M', 1, '2015-11-16 22:14:21', NULL, NULL),
		('Andrew', 'Patrick', 'Frazier', 'Alan', '281-92-3133', 3, 'M', 2, '2015-11-04 10:33:05', NULL, NULL),
		('Patricia', 'Robin', 'Little', 'Cheryl', '565-97-5948', 2, 'F', 3, '2016-02-22 07:23:48', NULL, NULL),
		('Jane', 'Sara', 'Bennett', 'Stephanie', '809-56-4451', 2, 'F', 2, '2015-09-06 05:18:40', NULL, NULL),
		('Paula', 'Lori', 'Williamson', 'Frances', '802-63-8862', 2, 'F', 4, '2016-01-21 20:15:52', NULL, NULL),
		('Christina', 'Wanda', 'Wells', 'Linda', '476-81-4631', 1, 'F', 1, '2015-12-24 18:43:41', NULL, NULL),
		('Richard', 'Jason', 'Barnes', 'Harry', '678-41-5599', 3, 'M', 4, '2016-05-13 00:29:40', NULL, NULL),
		('Fred', 'Roger', 'Gray', 'Jonathan', '697-97-8578', 1, 'M', 4, '2015-08-31 03:54:52', NULL, NULL),
		('Thomas', 'Sean', 'Alvarez', 'Jose', '141-81-3688', 1, 'M', 4, '2016-04-08 14:24:43', NULL, NULL),
		('George', 'Roger', 'Nichols', 'James', '570-89-3398', 1, 'M', 1, '2015-11-06 15:31:30', NULL, NULL),
		('Ralph', 'Harold', 'Ross', 'Chris', '846-86-8317', 2, 'M', 1, '2016-05-11 03:56:17', NULL, NULL),
		('Wanda', 'Theresa', 'Cruz', 'Anne', '456-46-2206', 1, 'F', 1, '2016-01-08 14:07:46', NULL, NULL),
		('Kathy', 'Dorothy', 'Robinson', 'Maria', '612-97-4108', 3, 'F', 3, '2016-01-02 02:48:36', NULL, NULL),
		('Jesse', 'Gregory', 'Hudson', 'Ronald', '431-08-0679', 3, 'M', 1, '2016-04-22 05:50:56', NULL, NULL),
		('Brandon', 'Ronald', 'Sullivan', 'Billy', '429-89-9373', 2, 'M', 2, '2015-09-01 09:23:32', NULL, NULL),
		('Tina', 'Lisa', 'Perry', 'Robin', '127-02-3543', 2, 'F', 4, '2016-03-23 07:56:37', NULL, NULL)
  `);
});

db.close();